#ifndef BMP_HANDLER_H
#define BMP_HANDLER_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

// Define PACKED macro for proper structure packing
#ifndef PACKED
    #ifdef _MSC_VER
        #define PACKED __pragma(pack(push, 1)) // For Microsoft Visual Studio compiler
    #else
        #define PACKED __attribute__((packed)) // For other compilers
    #endif
#endif

// Define PACKED_POP macro to revert structure packing
#ifndef PACKED_POP
    #ifdef _MSC_VER
        #define PACKED_POP __pragma(pack(pop)) // For Microsoft Visual Studio compiler
    #else
        #define PACKED_POP // For other compilers
    #endif
#endif


// Structure representing the header of a BMP file
struct PACKED bmp_header {
    uint16_t bfType;            // File type signature, should be 'BM' for BMP files
    uint32_t bfileSize;         // Size of the BMP file in bytes
    uint32_t bfReserved;        // Reserved; must be set to zero
    uint32_t bOffBits;          // Offset to the start of image data
    uint32_t biSize;            // Size of the header structure (usually 40 bytes)
    uint32_t biWidth;           // Width of the image in pixels
    uint32_t biHeight;          // Height of the image in pixels
    uint16_t biPlanes;          // Number of color planes, must be 1
    uint16_t biBitCount;        // Number of bits per pixel (1, 4, 8, 16, 24, or 32)
    uint32_t biCompression;     // Compression method being used (0 for uncompressed)
    uint32_t biSizeImage;       // Size of the raw image data (including padding)
    uint32_t biXPelsPerMeter;   // Horizontal resolution of the image (pixels per meter)
    uint32_t biYPelsPerMeter;   // Vertical resolution of the image (pixels per meter)
    uint32_t biClrUsed;         // Number of colors in the color palette (0 for full color images)
    uint32_t biClrImportant;    // Number of important colors used (usually ignored)
};
PACKED_POP // Revert structure packing after definition


// Enum representing different I/O statuses for BMP files
enum bmp_io_statuses {
    BMP_READ_OK = 0,                        
    BMP_READ_INVALID_SIGNATURE,             
    BMP_READ_INVALID_BITS,                  
    BMP_READ_INVALID_HEADER,                
    BMP_READ_INVALID_MEMORY_ALLOCATION,    
    BMP_READ_INVALID_BIT_COUNT,             
    BMP_READ_INVALID_PADDING,               
    BMP_WRITE_OK = 0                       
};


// Function to read from a BMP file and populate an Image structure
// Parameters:
//   in: FILE pointer to the input BMP file
//   img: Pointer to the Image structure to populate
// Returns:
//   An enum representing the read status
enum bmp_io_statuses read_from_bmp(FILE* in, struct Image* img);


// Function to write an Image structure to a BMP file
// Parameters:
//   out: FILE pointer to the output BMP file
//   img: Pointer to the Image structure to write
// Returns:
//   An enum representing the write status
enum bmp_io_statuses write_to_bmp(FILE* out, const struct Image* img);

#endif
