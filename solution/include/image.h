#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <malloc.h>
#include <stddef.h>


// Structure representing a pixel with RGB components
struct Pixel {
    uint8_t r, g, b;
};


// Structure representing an image with width, height, and pixel data
struct Image {
    uint64_t width, height; // Width and height of the image
    struct Pixel* data;     // Pointer to the pixel data array
};


// Function to create a new image with specified width and height
// Parameters:
//   width: Width of the image
//   height: Height of the image
// Returns:
//   The created image
struct Image create_image(const uint64_t width, const uint64_t height);


// Function to delete an image and free its memory
// Parameters:
//   img: Pointer to the image to delete
void delete_image(struct Image* img);

#endif
