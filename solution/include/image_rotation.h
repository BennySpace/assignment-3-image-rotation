#ifndef IMAGE_ROTATION_H
#define IMAGE_ROTATION_H

#include "image.h"


// Function to rotate an image
// Parameters:
//   source: The source image to rotate
// Returns:
//   The rotated image
struct Image rotate_image(const struct Image source);

#endif
