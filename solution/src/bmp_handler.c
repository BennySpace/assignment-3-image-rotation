#include "bmp_handler.h"


// Define constants for BMP header fields
#define BMP_HEADER_RESERVED 0
#define BMP_HEADER_SIZE 40
#define BMP_ALIGNMENT 4
#define BMP_ID 0x4D42
#define BMP_STRUCT_SIZE sizeof(struct bmp_header)
#define BMP_PLANES 1
#define BMP_BITS_PER_PIXEL 24
#define BMP_COMPRESSION_TYPE 0
#define BMP_X_PIXELS_PER_METER 2835
#define BMP_Y_PIXELS_PER_METER 2835
#define BMP_COLORS_QUANTITY 0
#define BMP_COLORS_IMPORTANT 0


// Calculate padding for each row in BMP image
uint8_t calculate_padding(uint64_t width) {
    uint64_t row_size = width * sizeof(struct Pixel);
    uint8_t padding = (BMP_ALIGNMENT - (row_size % BMP_ALIGNMENT)) % BMP_ALIGNMENT;
    
    return padding;
}


// Generate BMP header based on image dimensions
struct bmp_header generate_header(const struct Image* img) {
    uint64_t size = img->height * (img->width * BMP_STRUCT_SIZE + calculate_padding(img->width));
    uint32_t width = img->width;
    uint32_t height = img->height;

    struct bmp_header header = {0};

    header.bfType = BMP_ID;
    header.bfileSize = BMP_STRUCT_SIZE + size;
    header.bfReserved = BMP_HEADER_RESERVED;
    header.bOffBits = BMP_STRUCT_SIZE;
    header.biSize = BMP_HEADER_SIZE;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BMP_BITS_PER_PIXEL;
    header.biCompression = BMP_COMPRESSION_TYPE;
    header.biSizeImage = size;
    header.biXPelsPerMeter = BMP_X_PIXELS_PER_METER;
    header.biYPelsPerMeter = BMP_Y_PIXELS_PER_METER;
    header.biClrUsed = BMP_COLORS_QUANTITY;
    header.biClrImportant = BMP_COLORS_IMPORTANT;

    return header;
}


// Read from a BMP file and populate an Image structure
enum bmp_io_statuses read_from_bmp(FILE* in, struct Image* img) {
    struct bmp_header header = {0};

    if (!in || !img) return BMP_READ_INVALID_HEADER;

    // Read BMP header
    if (!fread(&header, BMP_STRUCT_SIZE, 1, in)) return BMP_READ_INVALID_HEADER;
    if (header.bfType != BMP_ID) return BMP_READ_INVALID_HEADER;
        
    // Create image structure
    *img = create_image(header.biWidth, header.biHeight);
    const uint8_t padding = calculate_padding(img->width);

    if (!img->data) return BMP_READ_INVALID_MEMORY_ALLOCATION;

    // Read image data row by row
    for (uint32_t h = 0; h < img->height; h++) {
        if (fread(img->data + img->width * h, sizeof(struct Pixel), img->width, in) != img->width) {
            return BMP_READ_INVALID_BITS;
        } else if (fseek(in, (long) padding, SEEK_CUR)) {
            return BMP_READ_INVALID_SIGNATURE;
        }
    }

    return BMP_READ_OK;
}


// Write an Image structure to a BMP file
enum bmp_io_statuses write_to_bmp(FILE* out, const struct Image* img) {
    struct bmp_header header = generate_header(img);

    if (!out || !img) return BMP_READ_INVALID_HEADER;
    
    // Write BMP header
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) return BMP_READ_INVALID_HEADER;

    const uint8_t padding = calculate_padding(img->width);
    uint64_t padding_value = 0;

    // Write image data row by row along with padding
    for (uint64_t h = 0; h < img->height; h++) {
        if (!fwrite(img->data + h * img->width, sizeof(struct Pixel), img->width, out)) {
            return BMP_READ_INVALID_BITS;
        } else if (!fwrite(&padding_value, sizeof(uint8_t), padding, out)) {
            return BMP_READ_INVALID_PADDING;
        }
    }

    return BMP_READ_OK;
}
