#include "image.h"


// Function to create an Image structure with the specified width and height
struct Image create_image(const uint64_t width, const uint64_t height) {
    // Allocate memory for pixel data
    struct Pixel* pixel_data = malloc(width * height * sizeof(struct Pixel));
    
    // Initialize the Image structure
    struct Image image = {
        .width = width,
        .height = height,
        .data = pixel_data
    };
    
    return image;
}


// Function to delete an Image structure and free its associated memory
void delete_image(struct Image* img) {
    free(img->data); // Free the memory allocated for pixel data
    img->data = NULL; // Set the data pointer to NULL to prevent dangling pointers
}
