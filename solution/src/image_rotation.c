#include "image_rotation.h"


// Rotate the image clockwise by 90 degrees
struct Image rotate_image(struct Image const source) {
    uint64_t new_height = source.height;
    uint64_t new_width = source.width;

    // Create a new image with swapped dimensions
    struct Image rotated = create_image(new_height, new_width);

    // Iterate through each pixel of the source image and assign it to the rotated image with adjusted coordinates
    for (uint64_t i = 0; i < new_height; i++) {
        for (uint64_t j = 0; j < new_width; j++) {
            // Rotate the coordinates (i, j) to (j, new_width - i - 1) for clockwise rotation
            rotated.data[(new_width - j - 1) * rotated.width + i] = source.data[i * new_width + j];
        }
    }
    
    return rotated;
}
