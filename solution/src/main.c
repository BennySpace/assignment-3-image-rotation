#include "main.h"
#include <stdbool.h>


// Function to ensure the angle is within the range [0, 360) and handles special cases
void check_angle(int *angle) {
    if (*angle < 0) {
        *angle += 360; // Adjust negative angles to be within [0, 360)
    } else if (*angle % 90 != 0) {
        printf("Invalid angle, allowed angles: 0, 90, -90, 180, -180, 270, -270");

        exit(2);
    }
} 


// Function to check if input and output files are valid
void check_io(FILE *in, FILE *out) {
    if (in == NULL) {
        printf("Invalid input file!");
        
        exit(3);
    } else if (out == NULL) {
        printf("Invalid output file!");

        exit(4);
    }
}


// Main function to read an image, apply transformations, and write the result
int main(int argc, char **argv) {
    struct Image img = {0}; 


    if (argc != 4) {
        printf("Wrong argument quantity! Format must be: <source-image> <transformed-image> <angle>");

        return 1; 
    }

    // Open input and output files
    FILE *in = fopen(argv[1], "rb");
    FILE *out = fopen(argv[2], "wb");

    int angle = atoi(argv[3]) % 360; // Calculate angle modulo 360 degrees

    check_angle(&angle); // Ensure angle is within valid range
    check_io(in, out); // Check if input and output files are valid

    // Read image data from the input file
    enum bmp_io_statuses read_status = read_from_bmp(in, &img);

    if (read_status != BMP_READ_OK) {
        printf("Something went wrong!");

        return 5; 
    }

    struct Image result_image = img; // Initialize the result image with the original image

    // Rotate the image by multiples of 90 degrees based on the specified angle
    for (int i = 0; i < angle / 90; i++) {
        struct Image temp = rotate_image(result_image); // Rotate the image
        delete_image(&result_image); // Delete the previous result image data
        result_image = temp; // Update the result image with the rotated image
    }

    // Write the transformed image data to the output file
    enum bmp_io_statuses write_status = write_to_bmp(out, &result_image);

    if (write_status != BMP_WRITE_OK) {
        printf("Something went wrong!");

        return 6; 
    }

    // Close input and output files
    if (fclose(in) == true) {
        printf("Failed to close file!");

        return 7; 
    } else if (fclose(out) == true) {
        printf("Failed to close result!");

        return 8; 
    }

    delete_image(&result_image); 

    return 0; 
}
